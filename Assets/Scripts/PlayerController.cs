﻿using System.Collections.Generic;
using Pieces;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	private Camera _camera;
	private ChessPiece _hoveredPiece;
	private ChessPiece _selectedPiece;
	private List<ChessTile> _highlihtedTiles;

	private void Start () 
	{
		_camera = Camera.main;
		_highlihtedTiles = new List<ChessTile>();
	}

	private void Update ()
	{
		//TODO: Change these names or reorganize these update functions
		UpdateHoveredPiece();
		UpdateSelectedPiece();
		UpdateSelectedTile();
	}

	private void UpdateSelectedTile()
	{
		if (!_selectedPiece || !Input.GetMouseButtonUp(0)) return;
		RaycastHit raycastHit;
		Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
		ChessTile selectedTile = null;
		if (Physics.Raycast(ray, out raycastHit)) 
			selectedTile = raycastHit.transform.gameObject.GetComponent<ChessTile>();
			
		if (!selectedTile || !_highlihtedTiles.Contains(selectedTile)) return;
		_selectedPiece.Move(selectedTile);
		DeselectPiece();
	}

	private void UpdateSelectedPiece()
	{
		if (!_hoveredPiece || !Input.GetMouseButtonUp(0)) return;
		SelectPiece(_hoveredPiece);	
	}

	private void DeselectPiece()
	{
		if (_selectedPiece == null) return;
		_selectedPiece.IsSelected = false;
		_selectedPiece = null;
		
		foreach (var highlihtedTile in _highlihtedTiles)
			highlihtedTile.IsHighlighted = false;
		_highlihtedTiles.Clear();
	}
	
	private void SelectPiece(ChessPiece targetPiece)
	{
		DeselectPiece();
		
		if (targetPiece == _selectedPiece) return;
		
		_selectedPiece = targetPiece;
		_selectedPiece.IsSelected = true;
		
		List<ChessTile> validMoves = _selectedPiece.GetValidMoves();
		_highlihtedTiles = validMoves;
		
		foreach (var highlihtedTile in _highlihtedTiles)
			highlihtedTile.IsHighlighted = true;	
	}

	private void UpdateHoveredPiece()
	{
		RaycastHit raycastHit;
		Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

		ChessPiece curretHoveredPiece = null;
		if (Physics.Raycast(ray, out raycastHit))
			curretHoveredPiece = raycastHit.transform.gameObject.GetComponent<ChessPiece>();

		if (curretHoveredPiece == _hoveredPiece) return;
		if (_hoveredPiece != null)
			_hoveredPiece.IsHighlighted = false;
	
		if (curretHoveredPiece != null)
			curretHoveredPiece.IsHighlighted = true;
	
		_hoveredPiece = curretHoveredPiece;
	}
}
