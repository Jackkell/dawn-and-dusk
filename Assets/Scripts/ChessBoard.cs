﻿using System.Collections;
using System.Collections.Generic;
using Pieces;
using UnityEngine;


[System.Serializable]
public struct PieceLocation
{
	public GameObject Piece;
	public Vector2Int Position;
	public bool IsWhite;
}


public class ChessBoard : MonoBehaviour
{
	private const int LetterAValue = 65;
	
	public Material BlackTileMaterial;
	public Material BlackPieceMaterial;
	public Material WhiteTileMaterial;
	public Material WhitePieceMaterial;
	
	public Vector2Int BoardSize = new Vector2Int(8, 8);
	public Vector2Int TileSize = new Vector2Int(1, 1);
	public Vector2 TileOffset = new Vector2(0, 0);
	
	[HideInInspector]
	public List<PieceLocation> PieceLocations = new List<PieceLocation>();

	public GameObject TilePrefab;

	public GameObject[,] Tiles;

	private void Awake()
	{
		ClearBoardImmediate();
	}

	private void Start()
	{
		GenerateBoard();
	}

	public void GenerateBoard()
	{	
		GenerateTiles();
		GeneratePieces();
	}
	
	public void ClearBoardImmediate()
	{
		Transform targetTransform = gameObject.transform;
		
		for (int i = targetTransform.childCount - 1; i >= 0; i--)
		{
			DestroyImmediate(targetTransform.GetChild(i).gameObject);
		}
	}

	private void GenerateTiles()
	{
		Tiles = new GameObject[BoardSize.x, BoardSize.y];
		for (int columnIndex = 0; columnIndex < BoardSize.y; columnIndex++)
		{
			for (int rowIndex = 0; rowIndex < BoardSize.x; rowIndex++)
			{
				AddTileToBoard(rowIndex, columnIndex);
			}
		}
	}

	private void GeneratePieces()
	{
		foreach (PieceLocation pieceLocation in PieceLocations)
		{
			Vector3 gridPosition = GetTilePosition(pieceLocation.Position.x, pieceLocation.Position.y);
			GameObject pieceInstance = Instantiate(pieceLocation.Piece,  gridPosition, Quaternion.identity);
			ChessPiece chessPiece = pieceInstance.GetComponent<ChessPiece>();
			chessPiece.IsWhite = pieceLocation.IsWhite;
			chessPiece.CurrentTile = Tiles[pieceLocation.Position.x, pieceLocation.Position.y].GetComponent<ChessTile>();
			chessPiece.CurrentTile.CurrentPiece = chessPiece;
			chessPiece.CurrentBoard = this;
			chessPiece.transform.parent = transform;
		}
	}

	private void AddTileToBoard(int rowIndex, int columnIndex)
	{
		Vector3 tilePosition = GetTilePosition(rowIndex, columnIndex);
		GameObject tileInstance = Instantiate(TilePrefab, tilePosition, Quaternion.identity);
		
		tileInstance.name = ConvertTileCoordinateToCode(rowIndex, columnIndex);
		tileInstance.transform.parent = transform;

		ChessTile chessTile = tileInstance.GetComponent<ChessTile>();
		chessTile.IsWhite = ConvertTileCoordinateToIsWhite(rowIndex, columnIndex);
		chessTile.Position = new Vector2Int(rowIndex, columnIndex);
		chessTile.Board = this;

		Tiles[rowIndex, columnIndex] = tileInstance;
	}
	
	private static string ConvertTileCoordinateToCode(int rowIndex, int columnIndex)
	{
		int rankNumber = rowIndex + 1;
		string fileLetter = ((char) (LetterAValue + columnIndex)).ToString();
		string tileCode = string.Format("{1}{0}", rankNumber, fileLetter);
		return tileCode;
	}

	private static bool ConvertTileCoordinateToIsWhite(int rowIndex, int columnIndex)
	{
		return (columnIndex + rowIndex) % 2 != 0;
	}
	
	private Vector3 GetTilePosition(int rowIndex, int columnIndex)
	{
		float xPosition = columnIndex * TileSize.y + TileOffset.y;
		float zPosition = rowIndex * TileSize.x + TileOffset.x;
		Vector3 tilePosition = new Vector3(xPosition, 0, zPosition);
		return tilePosition + transform.position;
	}

	public ChessTile GetTile(int row, int column)
	{
		if (0 > row || row >= Tiles.GetLength(0) || 0 > column || column >= Tiles.GetLength(1))
			return null;
		
		return Tiles[row, column].GetComponent<ChessTile>();
	}
}
