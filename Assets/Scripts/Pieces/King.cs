﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pieces
{
    public class King : ChessPiece
    {
        public int Range = 1;
        
        public override List<ChessTile> GetValidMoves()
        {
            var validTiles = new List<ChessTile>();
            for (int xIndex = -1; xIndex <= 1; xIndex++)
            {
                for (int yIndex = -1; yIndex <= 1; yIndex++)
                {
                    if (yIndex == 0 && xIndex == 0) continue;
                    Vector2Int direction = new Vector2Int(xIndex, yIndex);
                    validTiles = validTiles.Concat(GetUnBlockedTiles(direction, Range)).ToList();
                }
            }
            return validTiles;
        }
    }
}