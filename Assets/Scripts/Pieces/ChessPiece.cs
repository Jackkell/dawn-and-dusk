﻿using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Collections;

namespace Pieces
{
	public abstract class ChessPiece : MonoBehaviour
	{
		public int Value;
		public bool IsWhite;
		public GameObject Highlight;

		[ReadOnly] public bool HasMoved;
		[ReadOnly] public bool IsHighlighted;
		[ReadOnly] public bool IsSelected;
		[ReadOnly] public bool IsMoving;
		[ReadOnly] public ChessBoard CurrentBoard;
		[ReadOnly] public ChessTile CurrentTile; 

		private MeshRenderer _meshRenderer;
	
		public Vector2Int Position => CurrentTile.Position;

		private void Start () 
		{
			_meshRenderer = GetComponentInChildren<MeshRenderer>();
		}

		private void Update () 
		{
			UpdateTeamColor();
			UpdateHighlight();
			if (CurrentTile == null)
			{
				Destroy(gameObject);
			}
		}

		private void UpdateTeamColor()
		{
			if (CurrentBoard == null) return;

			_meshRenderer.material = IsWhite ? 
				CurrentBoard.WhitePieceMaterial : 
				CurrentBoard.BlackPieceMaterial;
		}

		private void UpdateHighlight()
		{
			if (Highlight == null) return;
		
			Highlight.SetActive(IsHighlighted);
		}

		public void SetIsHighlited(bool isHighlited)
		{
			IsHighlighted = isHighlited;
		}

		public void ToggleSelected()
		{
			IsSelected = !IsSelected;
		}

		public string GetColorName()
		{
			return IsWhite ? "White" : "Black";
		}

		public abstract List<ChessTile> GetValidMoves();

		public void Move(ChessTile destinationChessTile)
		{
			CurrentTile.CurrentPiece = null;
			destinationChessTile.CurrentPiece = this;
			CurrentTile = destinationChessTile;
			transform.position = CurrentTile.gameObject.transform.position;
			HasMoved = true;
		}

		protected ChessTile GetRelativeTile(Vector2Int offset)
		{
			return CurrentBoard.GetTile(offset.x + Position.x, offset.y + Position.y);
		}

		protected bool IsEnemy(ChessPiece otherPiece)
		{
			return otherPiece.IsWhite != IsWhite;
		}

		protected IEnumerable<ChessTile> GetUnBlockedTiles(Vector2Int direction, int range)
		{
			List<ChessTile> unBlockedTiles = new List<ChessTile>();
			for (int currentRange = 1; currentRange <= range; currentRange++)
			{
				ChessTile tile = GetRelativeTile(direction * currentRange);
                
				if (tile == null) break;
                
				if (tile.IsOccupied() == false) 
					unBlockedTiles.Add(tile);
				else if (IsEnemy(tile.CurrentPiece))
				{
					unBlockedTiles.Add(tile);
					break;
				} 
				else
					break;
			}

			return unBlockedTiles;
		}
	}
}
