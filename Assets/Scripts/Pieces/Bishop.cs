﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pieces
{  
    public class Bishop : ChessPiece
    {
        public int Range = 8;
        
        public override List<ChessTile> GetValidMoves()
        {
            var validMoves = new List<ChessTile>();
            var indexes = new List<int>() {-1, 1};
            foreach (var xindex in indexes)
            {
                foreach (var yIndex in indexes)
                {
                    var direction = new Vector2Int(xindex, yIndex);
                    validMoves = validMoves.Concat(GetUnBlockedTiles(direction, Range)).ToList();
                }
            }

            return validMoves;
        }
    }
}