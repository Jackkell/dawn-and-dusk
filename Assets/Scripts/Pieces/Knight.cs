﻿using System.Collections.Generic;
using UnityEngine;

namespace Pieces
{
    public class Knight : ChessPiece
    {
        public int Range = 2;
        public override List<ChessTile> GetValidMoves()
        {
            // TODO: Rewrite this and abstract out some of this functionality 
            var validTiles = new List<ChessTile>();
            for (int xIndex = -1; xIndex <= 1; xIndex++)
            {
                for (int yIndex = -1; yIndex <= 1; yIndex++)
                {
                    if (yIndex == xIndex || (yIndex != 0 && xIndex != 0)) continue;
                    Vector2Int direction = new Vector2Int(xIndex, yIndex);
                    // Moving right or left
                    if (Mathf.Abs(yIndex) > 0)
                    {
                        ChessTile upTile = GetRelativeTile(new Vector2Int(1, yIndex * Range));
                        ChessTile downTile = GetRelativeTile(new Vector2Int(-1, yIndex * Range));
                        if (upTile && (upTile.IsOccupied() == false || IsEnemy(upTile.CurrentPiece)))
                            validTiles.Add(upTile);
                        if (downTile && (downTile.IsOccupied() == false || IsEnemy(downTile.CurrentPiece)))
                            validTiles.Add(downTile);
                    }
                    // Moving up or down
                    else
                    {
                        ChessTile leftTile = GetRelativeTile(new Vector2Int(xIndex * Range, -1));
                        ChessTile rightTile = GetRelativeTile(new Vector2Int(xIndex * Range, 1));
                        if (leftTile && (leftTile.IsOccupied() == false || IsEnemy(leftTile.CurrentPiece)))
                            validTiles.Add(leftTile);
                        if (rightTile && (rightTile.IsOccupied() == false || IsEnemy(rightTile.CurrentPiece)))
                            validTiles.Add(rightTile);
                    }
                }
            }
            return validTiles;
        }
    }
}