﻿using System.Collections.Generic;
using UnityEngine;

namespace Pieces
{
	public class Pawn : ChessPiece
	{
		public override List<ChessTile> GetValidMoves()
		{
			var validMoves = new List<ChessTile>();
			
			ChessTile forwardTile = CurrentBoard.GetTile(Position.x + (IsWhite ? 1 : -1), Position.y);
			
			if (forwardTile && forwardTile.IsOccupied() == false)
			{
				validMoves.Add(forwardTile);
			
				ChessTile jumpTile = CurrentBoard.GetTile(Position.x + (IsWhite ? 2 : -2), Position.y);
				if (jumpTile && jumpTile.IsOccupied() == false && HasMoved == false)
					validMoves.Add(jumpTile);
			}

			ChessTile attackTile1 = CurrentBoard.GetTile(Position.x + (IsWhite ? 1 : -1), Position.y + 1);
			ChessTile attackTile2 = CurrentBoard.GetTile(Position.x + (IsWhite ? 1 : -1), Position.y - 1);

			if (attackTile1 && attackTile1.IsOccupied() && attackTile1.CurrentPiece.IsWhite != IsWhite)
				validMoves.Add(attackTile1);
			
			if (attackTile2 && attackTile2.IsOccupied() && attackTile2.CurrentPiece.IsWhite != IsWhite)
				validMoves.Add(attackTile2);
			
			return validMoves;
		}		
	}
}
