﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pieces
{
    public class Queen : ChessPiece
    {
        public int Range = 8;
        
        public override List<ChessTile> GetValidMoves()
        {
            var validMoves = new List<ChessTile>(); 
                
            for (int xIndex = -1; xIndex < 2; xIndex++)
            {
                for (int yIndex = -1; yIndex < 2; yIndex++)
                {
                    if (yIndex == 0 && xIndex == 0) continue;
                    var direction = new Vector2Int(xIndex, yIndex);
                    validMoves = validMoves.Concat(GetUnBlockedTiles(direction, Range)).ToList();
                }
                
            }

            return validMoves;
        }
    }
}