﻿using System.Collections;
using System.Collections.Generic;
using Pieces;
using UnityEngine;
using UnityEngine.Collections;

public class ChessTile : MonoBehaviour
{	
	public bool IsHighlighted;
	public bool IsWhite;

	public GameObject Highlight;

	private MeshRenderer _meshRenderer;
	
	[ReadOnly] public ChessBoard Board;
	[ReadOnly] public Vector2Int Position;
	[ReadOnly] public ChessPiece CurrentPiece; 

	private void Start()
	{
		_meshRenderer = GetComponentInChildren<MeshRenderer>();
	}

	private void Update()
	{
		UpdateTeamColor();
		UpdateHighlight();
	}

	private void UpdateTeamColor()
	{
		_meshRenderer.material = IsWhite ? 
			Board.WhiteTileMaterial : 
			Board.BlackTileMaterial;
	}

	private void UpdateHighlight()
	{
		Highlight.SetActive(IsHighlighted);
	}

	public bool IsOccupied()
	{
		return CurrentPiece != null;
	}
}
