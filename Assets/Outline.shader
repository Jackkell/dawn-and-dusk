﻿// Link to shared source code https://www.youtube.com/watch?v=SlTkBe4YNbo
// I have never written a shader before

Shader "N3k/Outline"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Outline ("Outline color", Color) = (0, 0, 0, 1)
		_OutlineWidth ("Outline width", Range(1.0,  5.0)) = 1.01 
	}
	
	CGINCLUDE
	#include "UnityCG.cginc"
	
	struct appdata
	{
	    float4 vertex : POSITION;
	    float3 normal : NORMAL;
	}
	
	struct v2f
	{
	    float4 pos : POSITION;
	    float4 color : COLOR;
	    float3 normal : NORMAL;
	}
	
	float _OutlineWidth;
	float4 _OutlineColor;
	
	v2f vert(appdata v)
	{
	    v.vertex.xyz *= _Outline;
	    
	    v2f o;
	    o.pos = UnityObjectToClipPos(v.vertex);
	    o.color = _OutlineColor;
	    return o;
	}
	
	ENDCG
	
	SubShader
	{
	    Pass
	    {
	        ZWrite Off
	        
	        CGPROGRAM
	        #pragam 
	        ENDCG
	    }
	    
	    Pass 
	    {
	    
	    }
	}
}
