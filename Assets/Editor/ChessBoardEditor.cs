﻿using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;


// Reference for how to make the reorderable list can be found here:
// http://va.lent.in/unity-make-your-lists-functional-with-reorderablelist/
// It is a really good blog that outlines how the reorderable list works because
// it is not that well documented by Unity because it is still an internal tool
namespace Editor
{
	[CustomEditor(typeof(ChessBoard))]
	public class ChessBoardEditor : UnityEditor.Editor
	{
		private const string PrefabExtension = ".prefab";
		private const string PiecePath = "Assets/Prefabs/Pieces/";
		private const string PawnPath = PiecePath + "Pawn" + PrefabExtension;
		private const string RookPath = PiecePath + "Rook" + PrefabExtension;
		private const string KnightPath = PiecePath + "Knight" + PrefabExtension;
		private const string BishopPath = PiecePath + "Bishop" + PrefabExtension;
		private const string QueenPath = PiecePath + "Queen" + PrefabExtension;
		private const string KingPath = PiecePath + "King" + PrefabExtension;
		
		private ReorderableList _list;
		private ChessBoard _chessBoard;
		
		private void OnEnable()
		{
			_chessBoard = (ChessBoard) target;
			
			// TODO: Find a way to add a warning when two pieces are added to the same square
			_list = new ReorderableList(serializedObject, serializedObject.FindProperty("PieceLocations"))
			{
				drawElementCallback = DrawElementCallback,
				drawHeaderCallback = DrawHeaderCallback,
				onAddCallback = OnAddCallback,
				onAddDropdownCallback = OnAddDropdownCallback,
			};
		}
		
		private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused) 
		{
			var element = _list.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;

			EditorGUI.PropertyField(
				new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("IsWhite"),
				GUIContent.none
			);
			EditorGUI.PropertyField(
				new Rect(rect.x + 30, rect.y, rect.width - 30 - 150, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("Piece"),
				GUIContent.none
			);
			EditorGUI.PropertyField(
				new Rect(rect.x + rect.width - 150, rect.y, 150, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("Position"),
				GUIContent.none
			);
		}

		private static void DrawHeaderCallback(Rect rect)
		{
			EditorGUI.LabelField(rect, "Piece Locations");
		}
		
		private static void OnAddCallback(ReorderableList l)
		{
			int index = l.serializedProperty.arraySize;
			l.serializedProperty.arraySize++;
			l.index = index;
			var element = l.serializedProperty.GetArrayElementAtIndex(index);
			element.FindPropertyRelative("IsWhite").boolValue = true;
			element.FindPropertyRelative("Piece").objectReferenceValue =
				AssetDatabase.LoadAssetAtPath(
					"Assets/Prefabs/Pieces/Pawn.prefab", 
					typeof(GameObject)) as GameObject;
			element.FindPropertyRelative("Position").vector2IntValue = new Vector2Int(0, 0);
		}
		
		private void OnAddDropdownCallback(Rect buttonRect, ReorderableList l)
		{
			var menu = new GenericMenu();
			var guids = AssetDatabase.FindAssets("", new[] {"Assets/Prefabs/Pieces"});
			foreach (var guid in guids)
			{
				var path = AssetDatabase.GUIDToAssetPath(guid);
				menu.AddItem(
					new GUIContent("Pieces/" + Path.GetFileNameWithoutExtension(path)),
					false, 
					clickHandler, 
					new PieceCreationParams(){ Path = path }
				);
			}
			menu.ShowAsContext();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			
			serializedObject.Update();
			_list.DoLayoutList();
			serializedObject.ApplyModifiedProperties();

			if (GUILayout.Button("Generate Board"))
			{
				_chessBoard.ClearBoardImmediate();
				_chessBoard.GenerateBoard();
			}

			if (GUILayout.Button("Clear Board"))
				_chessBoard.ClearBoardImmediate();

			// TODO: Fix this when this button is pressed the list of pieces positions should be cleared and a standard
			// TODO: board steup should be put in its place. It currently just add another set of pieces
			if (GUILayout.Button("Standard Board"))
			{
				RemoveAllPieces();
				AddStandradBoardPieces();
				_chessBoard.GenerateBoard();
			}

			if (GUILayout.Button("Remove All Pieces"))
			{
				RemoveAllPieces();
				_chessBoard.GenerateBoard();
			}
		}

		private struct PieceCreationParams
		{
			public string Path;
		}

		private void AddStandradBoardPieces()
		{
			AddTeam(isWhite: true);
			AddTeam(isWhite: false);
		}

		private void AddTeam(bool isWhite)
		{
			for (int columnIndex = 0; columnIndex < 8; columnIndex++)
				AddPiece(isWhite, PawnPath, new Vector2Int(isWhite ? 1 : 6, columnIndex));
			AddPiece(isWhite, RookPath, new Vector2Int(isWhite ? 0 : 7, 0));
			AddPiece(isWhite, RookPath, new Vector2Int(isWhite ? 0 : 7, 7));
			AddPiece(isWhite, KnightPath, new Vector2Int(isWhite ? 0 : 7, 1));
			AddPiece(isWhite, KnightPath, new Vector2Int(isWhite ? 0 : 7, 6));
			AddPiece(isWhite, BishopPath, new Vector2Int(isWhite ? 0 : 7, 2));
			AddPiece(isWhite, BishopPath, new Vector2Int(isWhite ? 0 : 7, 5));
			AddPiece(isWhite, QueenPath, new Vector2Int(isWhite ? 0 : 7, 3));
			AddPiece(isWhite, KingPath, new Vector2Int(isWhite ? 0 : 7, 4));
			
		}

		private void RemoveAllPieces()
		{
			_chessBoard.PieceLocations.Clear();
		}

		private void AddPiece(bool isWhite, string path, Vector2Int position)
		{
			var index = _list.serializedProperty.arraySize;
			_list.serializedProperty.arraySize++;
			_list.index = index;
			var element = _list.serializedProperty.GetArrayElementAtIndex(index);
			element.FindPropertyRelative("IsWhite").boolValue = isWhite;
			element.FindPropertyRelative("Piece").objectReferenceValue = 
				AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
			element.FindPropertyRelative("Position").vector2IntValue = position;
			serializedObject.ApplyModifiedProperties();
		}

		private void clickHandler(object clickedObject)
		{
			var data = (PieceCreationParams) clickedObject;
			var index = _list.serializedProperty.arraySize;
			_list.serializedProperty.arraySize++;
			_list.index = index;
			var element = _list.serializedProperty.GetArrayElementAtIndex(index);
			element.FindPropertyRelative("IsWhite").boolValue = true;
			element.FindPropertyRelative("Piece").objectReferenceValue = 
				AssetDatabase.LoadAssetAtPath(data.Path, typeof(GameObject)) as GameObject;
			element.FindPropertyRelative("Position").vector2IntValue = new Vector2Int();
			serializedObject.ApplyModifiedProperties();
		}
	}
}
